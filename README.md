# Employee Management

This is a employee management application that reads the employee records file, parses the file and displays the employee hierarchy.

##Instructions
This section contains the information about software requirements and running the application.

##Software Requirements
1. Java 8
2. Gradle
3. Test Cases require JUnit

##How to run application ?

1. Download the code from bitbucket and navigate to the project directory.
2. To build the project run "gradle clean install"
3. Executable JAR will be created in the "libs folder" under build directory inside project directory.
4. Navigate to libs folder.
5. Run the command in following format "java -jar employee-management-1.0-SNAPSHOT.jar <INPUT_FILE_LOCATION>".
6. Employee Hierarchy will be displayed on the console.

##Sample input file:

	alan,100,150
	martin,220,100
	jamie,150,1
	alex,275,100
	steve,400,150
	david,190,400

##Test Case Coverage:90%




