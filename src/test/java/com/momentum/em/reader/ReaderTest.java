package com.momentum.em.reader;

import java.util.List;

import com.momentum.em.reader.impl.FileReader;
import org.junit.Assert;
import org.junit.Test;

public class ReaderTest {

  private static final String FILE_NAME = "test.csv";
  private Reader reader = new FileReader();

  @Test
  public void shouldReturnLinesFromFile(){

    List<String> readLine = reader.read(getClass().getClassLoader().getResource(FILE_NAME).getPath().substring(1));
    Assert.assertEquals(1, readLine.size());
    Assert.assertEquals("vikas,10,10", readLine.get(0));
  }
}
