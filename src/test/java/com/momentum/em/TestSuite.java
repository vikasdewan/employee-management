package com.momentum.em;

import com.momentum.em.exception.BadRecordExceptionTest;
import com.momentum.em.exception.EMException;
import com.momentum.em.exception.EMExceptionTest;
import com.momentum.em.exception.InputFileNotFoundExceptionTest;
import com.momentum.em.exception.InvalidIdException;
import com.momentum.em.exception.InvalidIdExceptionTest;
import com.momentum.em.exception.InvalidNameExceptionTest;
import com.momentum.em.exception.InvalidRootEmployeeExceptionTest;
import com.momentum.em.parser.EmployeeRecordParserTest;
import com.momentum.em.reader.ReaderTest;
import com.momentum.em.service.impl.OrganizationServiceImplTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  BadRecordExceptionTest.class,
  EMExceptionTest.class,
  InvalidIdExceptionTest.class,
  InvalidNameExceptionTest.class,
  InputFileNotFoundExceptionTest.class,
  InvalidRootEmployeeExceptionTest.class,
  EmployeeRecordParserTest.class,
  ReaderTest.class,
  OrganizationServiceImplTest.class
})
public class TestSuite {
}
