package com.momentum.em.parser;

import com.momentum.em.exception.BadRecordException;
import com.momentum.em.exception.InvalidIdException;
import com.momentum.em.exception.InvalidNameException;
import com.momentum.em.model.Employee;
import com.momentum.em.parser.impl.EmployeeRecordParser;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeRecordParserTest {

  private final static String EMPLOYEE_NAME = "VIKAS";
  private final static String EMPLOYEE_ID = "1";
  private final static String MANAGER_ID = "100";
  private final static String MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER = "0";

  private Parser parser;

  @Before
  public void setup() {
    parser = new EmployeeRecordParser();
  }

  @After
  public void tearDown() {
    parser = null;
  }

  @Test
  public void shouldReturnEmployeeForCorrectEmployeeRecord() {

    String employeeRecord = EMPLOYEE_NAME + "," + EMPLOYEE_ID + "," + MANAGER_ID;
    Employee employee = parser.parse(employeeRecord);
    Assert.assertEquals(EMPLOYEE_NAME, employee.getName() );
    Assert.assertEquals(Long.valueOf(EMPLOYEE_ID), employee.getId());
    Assert.assertEquals(Long.valueOf(MANAGER_ID), employee.getManagerId());

  }

  @Test(expected = BadRecordException.class)
  public void shouldReturnAnErrorForEmptyEmployeeRecord() {
    String employeeRecord = "";
    parser.parse(employeeRecord);
  }

  @Test(expected = BadRecordException.class)
  public void shouldReturnAnErrorForNullEmployeeRecord() {

    String employeeRecord = null;
    parser.parse(employeeRecord);
  }

  @Test(expected = BadRecordException.class)
  public void shouldReturnAnErrorForIncompleteEmployeeRecord() {

    String employeeRecord = EMPLOYEE_NAME;
    parser.parse(employeeRecord);
  }

  @Test
  public void shouldReturnEmployeeWithNoManagerId() {

    String employeeRecord = EMPLOYEE_NAME + "," + EMPLOYEE_ID;
    Employee employee = parser.parse(employeeRecord);
    Assert.assertEquals(EMPLOYEE_NAME, employee.getName());
    Assert.assertEquals(Long.valueOf(EMPLOYEE_ID), employee.getId());
    Assert.assertEquals(Long.valueOf(MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER), employee.getManagerId());

  }

  @Test(expected = InvalidNameException.class)
  public void shouldReturnAnErrorForIncorrectNameInEmployeeRecord() {

    String employeeRecord = "12345" + "," + EMPLOYEE_ID + "," + MANAGER_ID;
    parser.parse(employeeRecord);
  }

  @Test(expected = InvalidIdException.class)
  public void shouldReturnAnErrorForIncorrectEmployeeId() {

    String employeeRecord = EMPLOYEE_NAME + "," + "123545ABCDEF" + "," + MANAGER_ID;
    parser.parse(employeeRecord);
  }

  @Test(expected = InvalidIdException.class)
  public void shouldReturnAnErrorForIncorrectManagerId() {

    String employeeRecord = EMPLOYEE_NAME + "," + EMPLOYEE_ID + "," + "12345ABCDEF_@";
    parser.parse(employeeRecord);
  }

}
