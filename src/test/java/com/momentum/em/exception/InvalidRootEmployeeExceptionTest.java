package com.momentum.em.exception;

import org.junit.Assert;
import org.junit.Test;

public class InvalidRootEmployeeExceptionTest {

  @Test
  public void shouldTestTheExceptionMessage(){
    String errorMessage = "abcd";
    InvalidRootEmployeeException exception = new InvalidRootEmployeeException();
    Assert.assertEquals("Invalid base employee", exception.getMessage());
  }
}
