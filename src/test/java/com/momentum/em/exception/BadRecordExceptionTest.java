package com.momentum.em.exception;

import org.junit.Assert;
import org.junit.Test;

public class BadRecordExceptionTest {

  @Test
  public void shouldTestExceptionMessage(){
    String badRecord = "abc,def,test";
    BadRecordException exception = new BadRecordException(badRecord);
    Assert.assertEquals("Invalid record " + badRecord, exception.getMessage() );
  }

}
