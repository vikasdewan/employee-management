package com.momentum.em.exception;

import org.junit.Assert;
import org.junit.Test;

public class InputFileNotFoundExceptionTest {

  @Test
  public void shouldTestTheExceptionMessage(){
    String errorMessage = "abcd";
    InputFileNotFoundException exception = new InputFileNotFoundException(errorMessage);
    Assert.assertEquals("File not found abcd", exception.getMessage());
  }
}
