package com.momentum.em.exception;

import org.junit.Assert;
import org.junit.Test;

public class InvalidNameExceptionTest {

  @Test
  public void shouldTestExceptionMessage() {
    String errorMessage = "123456";
    InvalidNameException exception = new InvalidNameException(errorMessage);
    Assert.assertEquals("Invalid name " + errorMessage, exception.getMessage());
  }
}
