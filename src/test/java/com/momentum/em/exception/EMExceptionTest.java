package com.momentum.em.exception;

import org.junit.Assert;
import org.junit.Test;

public class EMExceptionTest {

  @Test
  public void shouldTestTheExceptionMessage(){
    String errorMessage = "EM Exception occurred";
    EMException exception = new EMException(errorMessage);
    Assert.assertEquals(errorMessage, exception.getMessage());
  }
}
