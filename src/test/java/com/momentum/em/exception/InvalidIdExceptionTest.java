package com.momentum.em.exception;

import org.junit.Assert;
import org.junit.Test;

public class InvalidIdExceptionTest {

  @Test
  public void shouldTestExceptionMessage() {
    String errorMessage = "123456";
    InvalidIdException exception = new InvalidIdException(errorMessage);
    Assert.assertEquals("Invalid id " + errorMessage, exception.getMessage());
  }
}
