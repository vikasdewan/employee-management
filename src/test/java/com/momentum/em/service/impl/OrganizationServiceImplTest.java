package com.momentum.em.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.momentum.em.model.Employee;
import com.momentum.em.service.OrganizationService;
import com.momentum.em.service.impl.OrganizationServiceImpl;
import org.junit.Assert;
import org.junit.Test;

public class OrganizationServiceImplTest {

  private static final Long MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER = 0L;
  private static final Employee CEO = new Employee(150L, "Vikas", MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER);
  private static final Employee EMPLOYEE_1 = new Employee(230L, "Adam", 150L);
  private static final Employee EMPLOYEE_2 = new Employee(300L, "Brad", 150L);

  private static List<Employee> employees;
  private OrganizationService organizationService = new OrganizationServiceImpl();

  static {
    employees = new ArrayList<>();
    employees.add(CEO);
    employees.add(EMPLOYEE_1);
    employees.add(EMPLOYEE_2);
  }


  @Test
  public void shouldReturnCEO() {

    Employee ceo = organizationService.findCEO(employees);
    Assert.assertEquals(Long.valueOf(150L), ceo.getId());
    Assert.assertEquals(Long.valueOf(MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER), ceo.getManagerId());
    Assert.assertEquals("Vikas", ceo.getName());
  }

  @Test
  public void shouldReturnNULLIfThereIsNoEmployeeWithNoManager() {

    List<Employee> employees = new ArrayList<>();
    Employee employee1 = new Employee(230L, "Adam", 150L);
    Employee employee2 = new Employee(300L, "Brad", 150L);
    employees.add(employee1);
    employees.add(employee2);

    Employee ceo = organizationService.findCEO(employees);
    Assert.assertEquals(null, ceo);
  }

  @Test
  public void shouldReturnEmployeeHierarchy() {

    Employee baseEmployee = organizationService.populateHierarchyForEmployee(employees, CEO);

    Assert.assertEquals(Long.valueOf(150L), baseEmployee.getId());
    Assert.assertEquals(Long.valueOf(MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER), baseEmployee.getManagerId());
    Assert.assertEquals("Vikas", baseEmployee.getName());

    List<Employee> subordinates = baseEmployee.getSubordinates();
    Assert.assertEquals(2, subordinates.size());

    Employee employee1 = subordinates.get(0);
    Employee employee2 = subordinates.get(1);

    Assert.assertEquals(Long.valueOf(230L), employee1.getId());
    Assert.assertEquals(Long.valueOf(150L), employee1.getManagerId());
    Assert.assertEquals("Adam", employee1.getName());

    Assert.assertEquals(Long.valueOf(300L), employee2.getId());
    Assert.assertEquals(Long.valueOf(150L), employee2.getManagerId());
    Assert.assertEquals("Brad", employee2.getName());

  }

  @Test
  public void shouldReturnEmployeesWithInvalidManagers() {

    List<Employee> employees = new ArrayList<>();
    employees.add(CEO);
    Employee employee1 = new Employee(230L, "Adam", 150L);
    Employee employee2 = new Employee(300L, "Brad", 250L);
    employees.add(employee1);
    employees.add(employee2);

    List<Employee> employeesWithInvalidManagers = organizationService.findEmployeesWithInvalidManagers(employees);
    Employee employeeWithInvalidManager = employeesWithInvalidManagers.get(0);

    Assert.assertEquals(Long.valueOf(300L), employeeWithInvalidManager.getId());
    Assert.assertEquals(Long.valueOf(250L), employeeWithInvalidManager.getManagerId());
    Assert.assertEquals("Brad", employeeWithInvalidManager.getName());
  }


}
