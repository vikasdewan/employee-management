package com.momentum.em.parser.impl;

import com.momentum.em.exception.BadRecordException;
import com.momentum.em.exception.InvalidIdException;
import com.momentum.em.exception.InvalidNameException;
import com.momentum.em.model.Employee;
import com.momentum.em.parser.Parser;

public class EmployeeRecordParser implements Parser {

  private static final String MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER = "0";
  private static final String EMPLOYEE_RECORD_DELIMITER = ",";
  private static final String VALID_NAME_REGEX = "[a-zA-z]+([ '-][a-zA-Z]+)*";
  private static final String VALID_EMPLOYEE_ID_REGEX = "\\d+";

  @Override
  public Employee parse(String employeeRecordAsStr) {

    if (null == employeeRecordAsStr || employeeRecordAsStr.isEmpty()) {
      throw new BadRecordException(employeeRecordAsStr);
    }

    String[] employeeFieldsArr = employeeRecordAsStr.split(EMPLOYEE_RECORD_DELIMITER);

    if (employeeFieldsArr.length < 2) {
      throw new BadRecordException(employeeRecordAsStr);
    }

    String name = employeeFieldsArr[0];
    String employeeIdAsStr = employeeFieldsArr[1];
    String managerIdAsStr = MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER;

    if(employeeFieldsArr.length >=3) {
      managerIdAsStr = employeeFieldsArr[2];
      validateId(managerIdAsStr);
    }

    validateName(name);
    validateId(employeeIdAsStr);


    return new Employee(Long.valueOf(employeeIdAsStr), name, Long.valueOf(managerIdAsStr));
  }

  /**
   * This method validates the name. Name should only contain alphabets.
   * @param name
   */
  private void validateName(String name) {
    if (null == name || !name.matches(VALID_NAME_REGEX)) {
      throw new InvalidNameException(name);
    }
  }

  /**
   * This method validates that id should only be a number.
   * @param idAsStr
   */
  private void validateId(String idAsStr) {
    if (null == idAsStr || !idAsStr.matches(VALID_EMPLOYEE_ID_REGEX)) {
      throw new InvalidIdException(idAsStr);
    }
  }
}
