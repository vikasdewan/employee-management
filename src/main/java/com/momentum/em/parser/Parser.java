package com.momentum.em.parser;

import com.momentum.em.model.Employee;

public interface Parser {

  /**
   * This method will parse the employee record as string, validates it and will return the employee object.
   * @param employeeRecord
   * @return
   */
  Employee parse(String employeeRecord);
}
