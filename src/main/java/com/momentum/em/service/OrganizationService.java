package com.momentum.em.service;

import java.util.List;

import com.momentum.em.model.Employee;

public interface OrganizationService {

  /**
   * This method populates the hierarchy for an employee.
   * @param employees : list of employees
   * @param baseEmployee : root employee for whom hierarchy has to be populated.
   * @return
   */
  Employee populateHierarchyForEmployee(List<Employee> employees, Employee baseEmployee);

  /**
   * This method finds all employees with invalid managers. Invalid managers are those who did not exist in the system.
   * @param employees : list of employees.
   * @return
   */
  List<Employee> findEmployeesWithInvalidManagers(List<Employee> employees);

  /**
   * This method will return the root employee from the employee list.
   * @param employees : list of employees.
   * @return
   */
  Employee findCEO(List<Employee> employees);

}
