package com.momentum.em.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.momentum.em.exception.InvalidRootEmployeeException;
import com.momentum.em.model.Employee;
import com.momentum.em.service.OrganizationService;

public class OrganizationServiceImpl implements OrganizationService {

  private static final Long MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER = 0L;

  @Override
  public Employee findCEO(List<Employee> employees) {
    for (Employee employee : employees) {
      if (MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER.equals(employee.getManagerId())) {
        return employee;
      }
    }
    return null;
  }

  @Override
  public Employee populateHierarchyForEmployee(List<Employee> employees, Employee baseEmployee) {

    if(baseEmployee == null) {
      throw new InvalidRootEmployeeException();
    }

    List<Employee> subordinates = findSubordinates(employees, baseEmployee.getId());
    baseEmployee.setSubordinates(subordinates);

    for (Employee mgr : subordinates) {
      populateHierarchyForEmployee(employees, mgr);
    }

    return baseEmployee;
  }

  @Override
  public List<Employee> findEmployeesWithInvalidManagers(List<Employee> employees) {

    List<Employee> invalidEmployees = new ArrayList<>();

    employees.forEach(employee -> {
      if (employee.getManagerId() != MANAGER_ID_FOR_EMPLOYEE_WITH_NO_MANAGER && !validEmployee(employees, employee.getManagerId())) {
        invalidEmployees.add(employee);
      }
    });
    return invalidEmployees;

  }

  /**
   * This method find subordinates of a particular employee.
   *
   * @param managerId
   * @return
   */
  private List<Employee> findSubordinates(List<Employee> employees, long managerId) {
    List<Employee> subordinates = new ArrayList<>();

    for (Employee employee : employees) {
      if (0 != employee.getManagerId() && employee.getManagerId() == managerId) {
        subordinates.add(employee);
      }
    }
    return subordinates;
  }

  private boolean validEmployee(List<Employee> employees, long employeeId) {
    boolean isValidEmployee = false;

    for (Employee employee : employees) {
      if (employee.getId() == employeeId) {
        isValidEmployee = true;
        break;
      }
    }
    return isValidEmployee;
  }
}
