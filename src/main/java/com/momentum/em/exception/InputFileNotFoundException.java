package com.momentum.em.exception;

public class InputFileNotFoundException extends EMException {

  public InputFileNotFoundException(String fileName) {
    super("File not found " + fileName);
  }
}
