package com.momentum.em.exception;

public class InvalidNameException extends EMException {

  public InvalidNameException(String name) {
    super("Invalid name " + name);
  }
}
