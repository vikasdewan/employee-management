package com.momentum.em.exception;

public class InvalidIdException extends EMException {

  public InvalidIdException(String id) {
    super("Invalid id " + id);
  }
}
