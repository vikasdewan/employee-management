package com.momentum.em.exception;

public class InvalidRootEmployeeException extends EMException {

  public InvalidRootEmployeeException() {
    super("Invalid base employee");
  }
}
