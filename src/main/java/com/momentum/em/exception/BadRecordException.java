package com.momentum.em.exception;

public class BadRecordException extends EMException{

  public BadRecordException(String record) {
    super("Invalid record " + record);
  }
}
