package com.momentum.em.exception;

/**
 * This is the base exception for Employee Management Application.
 */
public class EMException extends RuntimeException {

  public EMException(String message) {
    super(message);
  }
}
