package com.momentum.em.model;

import java.util.List;

/**
 * This is a pojo class to represent employee.
 */
public class Employee {

  private Long id;
  private String name;
  private Long managerId;
  private List<Employee> subordinates;

  public Employee(Long id, String name, Long managerId) {
    this.id = id;
    this.name = name;
    this.managerId = managerId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getManagerId() {
    return managerId;
  }

  public void setManagerId(Long managerId) {
    this.managerId = managerId;
  }

  public List<Employee> getSubordinates() {
    return subordinates;
  }

  public void setSubordinates(List<Employee> subordinates) {
    this.subordinates = subordinates;
  }

  @Override
  public String toString() {
    return "Employee{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", managerId=" + managerId +
      ", subordinates=" + subordinates +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Employee employee = (Employee) o;

    if (id != employee.id) return false;
    if (managerId != employee.managerId) return false;
    return name != null ? name.equals(employee.name) : employee.name == null;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (int) (managerId ^ (managerId >>> 32));
    return result;
  }
}
