package com.momentum.em.reader.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.momentum.em.exception.InputFileNotFoundException;
import com.momentum.em.reader.Reader;

public class FileReader implements Reader {

  @Override
  public List<String> read(String fileName){

    List<String> readLines = new ArrayList<>();

    try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
      readLines = stream.collect(Collectors.toList());
    }
    catch(IOException e){
      throw new InputFileNotFoundException(fileName);
    }

    return readLines;
  }
}
