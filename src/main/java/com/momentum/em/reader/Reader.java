package com.momentum.em.reader;

import java.io.IOException;
import java.util.List;

public interface Reader {

  /**
   * This method reads the file line by line and returns the content in List.
   * @param fileName : name of the file that has to be read.
   * @return
   */
  List<String> read(String fileName);
}
