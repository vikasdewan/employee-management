package com.momentum.em;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.momentum.em.exception.EMException;
import com.momentum.em.model.Employee;
import com.momentum.em.parser.Parser;
import com.momentum.em.parser.impl.EmployeeRecordParser;
import com.momentum.em.reader.impl.FileReader;
import com.momentum.em.service.OrganizationService;
import com.momentum.em.service.impl.OrganizationServiceImpl;

/**
 * This is an entry point class for employee management application.
 * Employee records are loaded from the employee.csv file in the resource folder.
 */
public class Application {

  private OrganizationService organizationService = new OrganizationServiceImpl();
  private FileReader reader = new FileReader();;
  private Parser parser = new EmployeeRecordParser();

  public static void main(String[] args) {

    Application app = new Application();

    if(args.length == 0){
      System.err.println("please specify the file location, exiting application");
      return;
    }

    String fileName = args[0];

    if(fileName == null || fileName.isEmpty()){
      System.err.println("please specify the file location, exiting application");
      return;
    }

    try {

      app.run(fileName);

    }catch(EMException exception){
      System.err.println(exception.getMessage());

    }catch(Exception e) {
      System.err.println("Unknown error occured, exiting application.");
    }
  }

  private void run(String fileName){
    List<String> records = reader.read(fileName);
    List<Employee> employees =  loadEmployeesList(records);

    if(employees.isEmpty()){
      System.err.println("No employees record found.");
      return;
    }

    Employee ceo = organizationService.findCEO(employees);

    /*Display hierarchy*/
    Employee baseEmployee = organizationService.populateHierarchyForEmployee(employees, ceo);
    displayHierarchyForEmployee(baseEmployee);

    /*Display employees with invalid managers*/
    List<Employee> employeesWithInvalidManagers = organizationService.findEmployeesWithInvalidManagers(employees);
    displayEmployeesWithInvalidManagers(employeesWithInvalidManagers);
  }

  private List<Employee> loadEmployeesList(List<String> employeeRecords){
    List<Employee> employees =  new ArrayList<>();
    employeeRecords.forEach(record -> {
      try {
        employees.add(parser.parse(record));
      }catch (EMException exception){
        System.err.println(exception.getMessage());
      }
    });

    return employees;
  }

  private void displayHierarchyForEmployee(Employee employee){

    System.out.println(employee.getName());
    employee.getSubordinates().forEach( subordinate ->
      displayHierarchyForEmployee(subordinate)
    );
  }

  private void displayEmployeesWithInvalidManagers(List<Employee> employeesWithInvalidManagers){
    System.out.println("****Employees with Invalid Managers");
    employeesWithInvalidManagers.forEach( employee ->
    System.out.println(employee.getName()));
  }

}
